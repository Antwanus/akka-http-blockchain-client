package com.antoonvereecken;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.HttpsConnectionContext;
import akka.http.javadsl.OutgoingConnection;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.antoonvereecken.model.BlockChain;

import java.util.concurrent.CompletionStage;

public class AppWithConnectionLevelAPI {

   public void run() {
      ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");

      Flow<HttpRequest, HttpResponse, CompletionStage<OutgoingConnection>> connectionFlow =
          Http.get(actorSystem)
//              .outgoingConnection("localhost:8080")                         // non-HTTPS
//              .outgoingConnection(ConnectHttp.toHostHttps("localhost"))     // HTTPS with trusted certificate
          .outgoingConnection(
              ConnectHttp  .toHostHttps("localhost")
                           .withCustomHttpsContext(
                               HttpsConnectionContext.httpsClient(UntrustedServerCertificateUtility.getSSLContext())
                           )
      );

      Source<HttpRequest, NotUsed> source = Source.single(  HttpRequest.create()
                                                                        .withMethod(HttpMethods.GET)
                                                                        .withUri("/api/blockchain")
      );

      Unmarshaller<HttpEntity, BlockChain> blockChainUnmarshaller = Jackson.unmarshaller(BlockChain.class);
      Sink<HttpResponse, CompletionStage<Done>> sink = Sink.foreach(response -> {
         CompletionStage<BlockChain> blockChainCS = blockChainUnmarshaller.unmarshal(response.entity(), actorSystem);
         blockChainCS.whenComplete( (blockChain, throwable)-> {
            if (throwable != null)  System.out.println("THROWABLE: " +throwable);
            else  blockChain.printAndValidate();
         });
      });
      source.via(connectionFlow).to(sink).run(actorSystem);
   }


}

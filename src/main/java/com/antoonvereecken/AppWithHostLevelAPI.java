package com.antoonvereecken;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.ConnectionContext;
import akka.http.javadsl.HostConnectionPool;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.japi.Pair;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.antoonvereecken.model.Transaction;
import com.antoonvereecken.model.TransactionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.util.Try;

import java.util.Map;
import java.util.concurrent.CompletionStage;

public class AppWithHostLevelAPI {

   public void run() {
      ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");

      Flow< Pair<HttpRequest, Integer>,   Pair<Try<HttpResponse>, Integer>,   HostConnectionPool> connectionFlow =
          Http.get(actorSystem)
              .cachedHostConnectionPoolHttps(
                  ConnectHttp.toHostHttps("localhost")
                      .withCustomHttpsContext(
                          ConnectionContext.httpsClient(UntrustedServerCertificateUtility.getSSLContext())
                      )
              );
      Map<Integer, Transaction> transactions = Map.of(
          567, new Transaction(0, System.currentTimeMillis(), 123, 11),
          513, new Transaction(0, System.currentTimeMillis(), 234, 22),
          566, new Transaction(0, System.currentTimeMillis(), 345, 33),
          561, new Transaction(0, System.currentTimeMillis(), 456, 44),
          562, new Transaction(0, System.currentTimeMillis(), 567, 55),
          563, new Transaction(0, System.currentTimeMillis(), 678, 66),
          564, new Transaction(0, System.currentTimeMillis(), 789, 77),
          565, new Transaction(0, System.currentTimeMillis(), 890, 88),
          512, new Transaction(0, System.currentTimeMillis(), 901, 99)
          );
      Source<Integer, NotUsed> source = Source.from(transactions.keySet());

      Flow<Integer, Pair<HttpRequest, Integer>, NotUsed> getTxFlow = Flow.of(Integer.class)
          .map(i -> {
             Transaction t = transactions.get(i);
             HttpRequest request = HttpRequest.create()
                                          .withMethod(HttpMethods.POST)
                                          .withUri("/api/transaction")
                                          .withEntity( HttpEntities.create(
                                                      ContentTypes.APPLICATION_JSON,
                                                      new ObjectMapper().writeValueAsString(t)));
             return new Pair<>(request, i);
          });



      Unmarshaller<HttpEntity, TransactionResponse> txResponseUnmarshaller = Jackson
                                                                               .unmarshaller(TransactionResponse.class);

      Sink<Pair<Try<HttpResponse>, Integer>, CompletionStage<Done>> sink = Sink.foreach(response -> {
         System.out.println("got a response");
         if (response.first().isFailure())      System.out.println("Something went wrong " + response.first());
         else {
            Integer key = response.second();

            CompletionStage<TransactionResponse> responseFuture = txResponseUnmarshaller.unmarshal(
                response.first().get().entity(),      actorSystem
            );
            responseFuture.whenComplete( (txResponse, throwable) -> {
               if (throwable != null)     System.out.println("THROWABLE " + throwable);
               else {
                  transactions.get(key).setId(txResponse.getTransactionId());
                  System.out.println("for key " + key + " the ID is " + txResponse.getTransactionId());
               }
            });
         }
      });


      source.via(getTxFlow).via(connectionFlow).to(sink).run(actorSystem);
   }


}

package com.antoonvereecken;

public class Main {
   public static void main(String[] args) {
//      AppWithConnectionLevelAPI app = new AppWithConnectionLevelAPI();
//      AppWithHostLevelAPI app = new AppWithHostLevelAPI();
      AppWithRequestLevelAPI app = new AppWithRequestLevelAPI();
      app.run();
   }
}
